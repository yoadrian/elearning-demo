# Elearning Demo Course

A demo course using different elements of Articulate 360.

**Access the interactive course demo here**: https://yoadrian.gitlab.io/elearning-demo

## Summary of Course Content

Summary of elements used:
* Lesson 1: text, lists. Interactivity: tabs, expandable text, image hotspots
* Lesson 2: text, table, bar chart. Interactivity: process, scenario, sorting activity
* Lesson 3: text, audio, line chart, gallery. Interactivity: timeline, flashcard grid and stack
* Lesson 4: video, pdf attachment, pie chart, quote carousel. Interactive buttons
* Quiz: fill in the blank, multiple choice, multiple response, matching question, image uploads